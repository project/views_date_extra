<?php

/**
 * @author "Ahmad Hejazee"
 */

namespace Drupal\views_date_extra;

/**
 * Class DateTools
 *
 * @package Drupal\views_date_extra
 */
class DateTools {

  /**
   * Parse and translate a specially formatted date string to timestamp
   *
   * @param string $str
   * The relavtive date string.
   * Examples:
   *  - thisweek:saturday:start
   *  - nextweek:friday:end
   *
   * @return false|int
   */
  public static function calculateRelativeDateStr(string $str) {
    $parsed = static::parseRelativeDateStr($str);

    if ($parsed === FALSE) {
      return FALSE;
    }

    // get today's day of week
    $today_dow = static::getTodayDoW();

    // convert the request dow to number
    $weekday = static::convertDoWtoNumber($parsed['weekday']);

    // calculate diff
    if ($parsed['week'] == 'thisweek') {
      $diff = $weekday - $today_dow;
    }
    elseif ($parsed['week'] == 'nextweek') {
      $diff = $weekday - $today_dow + 7;
    }

    // calculate time str
    $time_str = '';
    if ($parsed['time'] == 'start') {
      $time_str = '00:00:00';
    }
    elseif ($parsed['time'] == 'end') {
      $time_str = '23:59:59';
    }

    $result = strtotime("today {$diff} days {$time_str}");

    return $result;
  }

  /**
   * parse 'relativedate' string (if valid) or return false (if invalid)
   *
   * @param string $str
   * The input string
   *
   * @return array|false
   */
  public static function parseRelativeDateStr(string $str) {
    $str = strtolower($str);
    $valid_weeks = ['thisweek', 'nextweek'];
    $valid_weekdays = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
    $valid_times = ['start', 'end'];

    [$week, $weekday, $time] = explode(':', $str);
    if ((in_array($week, $valid_weeks)) && (in_array($weekday, $valid_weekdays)) && (in_array($time, $valid_times))) {
      return [
        'week'    => $week,
        'weekday' => $weekday,
        'time'    => $time,
      ];
    }
    else {
      return FALSE;
    }
  }

  /**
   * convert day of week to number
   */
  private static function convertDoWtoNumber(string $str): int {
    $str = strtolower($str);
    switch ($str) {
      case 'sat': case 'saturday':  return 1;
      case 'sun': case 'sunday':    return 2;
      case 'mon': case 'monday':    return 3;
      case 'tue': case 'tuesday':   return 4;
      case 'wed': case 'wednesday': return 5;
      case 'thu': case 'thursday':  return 6;
      case 'fri': case 'friday':    return 7;
    }
  }

  /**
   * Get today's day of week (1 = saturday, 7=friday)
   */
  private static function getTodayDoW(): int {
    return static::convertDoWtoNumber(date('D'));
  }
}
