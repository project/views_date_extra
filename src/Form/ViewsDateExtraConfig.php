<?php

namespace Drupal\views_date_extra\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ViewsDateExtraConfig
 */
class ViewsDateExtraConfig extends ConfigFormBase {

  const SETTINGS = 'views_date_extra.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_date_extra_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config(static::SETTINGS);

    $form['views_date_extra'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Views Date Extra Settings'),
      '#description' => $this->t('NOTE: The above options affect the internal behavior of views_date_extra module.')
    ];

    $form['views_date_extra']['replace_date_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace all date fields\' handlers with views_date_extra handler.'),
      '#description' => $this->t('If unchecked, you can set handlers yourself in hook_views_data().'),
      '#default_value' => $settings->get('replace_date_fields'),
    ];

    $form['views_date_extra']['use_query_substitutions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use views query substitutions.'),
      '#description' => $this->t('Enabling this option, allows for views query caching but makes queries a little complex.'),
      '#default_value' => $settings->get('use_query_substitutions'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('replace_date_fields',     $form_state->getValue('replace_date_fields'))
      ->set('use_query_substitutions', $form_state->getValue('use_query_substitutions'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
