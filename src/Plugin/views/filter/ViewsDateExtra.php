<?php

namespace Drupal\views_date_extra\Plugin\views\filter;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\views\Annotation\ViewsFilter;
use Drupal\views\Plugin\views\filter\Date as ViewsFilterDate;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views_date_extra\DateTools;
use Drupal\views_date_extra\Form\ViewsDateExtraConfig;

/**
 * Views filter plugin which handles special cases.
 *
 * @ViewsFilter("views_date_extra")
 */
class ViewsDateExtra extends ViewsFilterDate {

  /**
   * @var array Holds week days with their index.
   */
  private array $weekdays;
  private array $weekdaysTranslated;

  private ImmutableConfig $viewsDateExtraSettings;

  /**
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->viewsDateExtraSettings = \Drupal::config(ViewsDateExtraConfig::SETTINGS);

    // this array is copied from system module (Drupal\system\Form\RegionalForm\buildForm())
    $this->weekdays = [0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday'];
    $this->weekdaysTranslated = array_map('t', $this->weekdays);

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // value is already set up properly, we're just adding our new field to it.
    $options['value']['contains']['first_day_of_week']['default'] = 'drupal';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    if (isset($form['value']['type']['#options'])) {
      $form['value']['type']['#options']['strtotime']    = $this->t('Put here any date string that php strtodate() supports!');
      $form['value']['type']['#options']['relativedate'] = $this->t('Special date strings: "thisweek:saturday:start" or "thisweek:friday:start", etc...');

      $form['value']['first_day_of_week'] = [
        '#type' => 'select',
        '#title' => $this->t('First day of week'),
        '#options' => [
            'drupal' => $this->t('Obtain from Regional settings'),
          ] + $this->weekdaysTranslated,
        '#default_value' => isset($this->value['first_day_of_week']) ? $this->value['first_day_of_week'] : 'drupal',
        '#states' => [
          'visible' => [
            ':input[name="options[value][type]"]' => ['value' => 'relativedate'],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateValidTime(&$form, FormStateInterface $form_state, $operator, $value) {
    $operators = $this->operators();

    if ($value['type'] == 'relativedate') {
      if ($operators[$operator]['values'] == 1) {
        $convert =  DateTools::calculateRelativeDateStr($value['value']);
        if (!empty($form['value']) && ($convert === FALSE)) {
          $form_state->setError($form['value'], $this->t('Relative date string not accepted (invalid).'));
        }
      }
      elseif ($operators[$operator]['values'] == 2) {
        $min = DateTools::calculateRelativeDateStr($value['min']);
        $max = DateTools::calculateRelativeDateStr($value['max']);

        if ($min === FALSE) {
          $form_state->setError($form['min'], $this->t('Relative date string not accepted (invalid).'));
        }
        if ($max === FALSE) {
          $form_state->setError($form['max'], $this->t('Relative date string not accepted (invalid).'));
        }
      }
    }
    elseif ($value['type'] == 'strtotime') {
      // No special handling, use parent logic
      parent::validateValidTime($form, $form_state, $operator, $value);
    }
    else {
      parent::validateValidTime($form, $form_state, $operator, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opBetween($field) {
    $use_query_substitutions = $this->viewsDateExtraSettings->get('use_query_substitutions');

    if ($this->value['type'] == 'relativedate') {
      if ($use_query_substitutions) {
        $a = "***VIEWS_DATE_EXTRA(filter|##|relativedate|##|{$this->value['min']})***";
        $b = "***VIEWS_DATE_EXTRA(filter|##|relativedate|##|{$this->value['max']})***";
      }
      else {
        $a = DateTools::calculateRelativeDateStr($this->value['min']);
        $b = DateTools::calculateRelativeDateStr($this->value['max']);
      }

      $operator = strtoupper($this->operator);
      $this->query->addWhereExpression($this->options['group'], "$field $operator $a AND $b");
    }
    elseif ($this->value['type'] == 'strtotime') {
      if ($use_query_substitutions) {
        $a = "***VIEWS_DATE_EXTRA(filter|##|strtotime|##|{$this->value['min']})***";
        $b = "***VIEWS_DATE_EXTRA(filter|##|strtotime|##|{$this->value['max']})***";
      }
      else {
        $request_time = \Drupal::time()->getRequestTime();
        $a = intval(strtotime($this->value['min'], $request_time));
        $b = intval(strtotime($this->value['max'], $request_time));
      }

      $operator = strtoupper($this->operator);
      $this->query->addWhereExpression($this->options['group'], "$field $operator $a AND $b");
    }
    else {
      parent::opBetween($field);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($field) {
    $use_query_substitutions = $this->viewsDateExtraSettings->get('use_query_substitutions');

    if ($this->value['type'] == 'relativedate') {
      if ($use_query_substitutions) {
        $value = "***VIEWS_DATE_EXTRA(filter|##|relativedate|##|{$this->value['value']})***";
      }
      else {
        $value = DateTools::calculateRelativeDateStr($this->value['value']);
      }

      $operator = strtoupper($this->operator);
      $this->query->addWhereExpression($this->options['group'], "$field $operator $value");
    }
    elseif ($this->value['type'] == 'strtotime') {
      if ($use_query_substitutions) {
        $value = "***VIEWS_DATE_EXTRA(filter|##|strtotime|##|{$this->value['value']})***";
      }
      else {
        $request_time = \Drupal::time()->getRequestTime();
        $value = intval(strtotime($this->value['value'], $request_time));
      }

      $this->query->addWhereExpression($this->options['group'], "$field $this->operator $value");
    }
    else {
      parent::opSimple($field);
    }
  }

  /**
   * Get first day of week according to what user has configured in the filter.
   *
   * @param $configured
   *
   * @return string Untranslated name of the week day
   */
  private function getFirstDayOfWeek($configured) {
    if ($configured == 'drupal') {
      $first_day = \Drupal::config('system.date')->get('first_day');
    }
    else {
      $first_day = $configured;
    }

    return $this->weekdays[$first_day];
  }
}
