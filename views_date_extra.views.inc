<?php

/**
 * Implements hook_views_data_alter().
 */
function views_date_extra_views_data_alter(array &$data) {
  foreach ($data as $table => $table_def) {
    foreach ($table_def as $field => $field_def) {
      if ($field == 'table') {
        continue;
      }

      if (isset($field_def['filter']['id']) && $field_def['filter']['id'] == 'date') {
        $data[$table][$field]['filter']['id'] = 'views_date_extra';
      }
    }
  }
}
