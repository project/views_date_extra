<?php

/**
 * @author "Ahmad Hejazee"
 */

use Drupal\views_date_extra\DateTools;
use Drupal\views\ViewExecutable;
use Drupal\views_date_extra\Form\ViewsDateExtraConfig;

/**
 * Implements hook_views_query_substitutions().
 */
function views_date_extra_views_query_substitutions(ViewExecutable $view) {
  $config = \Drupal::config(ViewsDateExtraConfig::SETTINGS);

  if ($config->get('use_query_substitutions')) {

    $patterns_used = [];

    // find all patterns in the views query

    // look for substitutions of the 'views_date_extra' filter handler
    foreach ($view->query->where as $where) {
      foreach ($where['conditions'] as $condition) {
        // since 'views_date_extra' adds its conditions using the addWhereExpression() method
        // so the operator is always set to 'formula'
        if ($condition['operator'] == 'formula') {
          // the substitution is placed in the 'field' key
          $string = $condition['field'];

          // check if it has a supported substitution
          $pattern = '/\*{3}VIEWS_DATE_EXTRA\((filter)\|\#\#\|(strtotime|relativedate)\|\#\#\|(.*)\)\*{3}/U';
          $matches = array();
          if (preg_match_all($pattern, $string, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
              // the full pattern that matched.
              $matched_pattern = $match[0];

              // always 'filter'
              $type = $match[1];

              // either strtotime or relativedate
              $func = $match[2];

              // the date string
              $str  = $match[3];

              $patterns_used[$matched_pattern] = [
                'type' => $type,
                'func' => $func,
                'str'  => $str,
              ];
            }
          }
        }
      }
    }


    //dpm($patterns_used);

    $substitutions = [];
    foreach ($patterns_used as $pattern => $def) {
      if ($def['type'] == 'filter') {
        if ($def['func'] == 'strtotime') {
          $substitutions[$pattern] = strtotime($def['str']);
        }
        elseif ($def['func'] == 'relativedate') {
          // TODO: pass first day of week.
          $substitutions[$pattern] = DateTools::calculateRelativeDateStr($def['str']);
        }
      }
    }
  }

  // dpm($substitutions);

  return $substitutions;
}

